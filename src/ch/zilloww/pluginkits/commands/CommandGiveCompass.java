package ch.zilloww.pluginkits.commands;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CommandGiveCompass implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
		
		if(commandSender instanceof Player) {
			
			Player player = (Player) commandSender;
			
			player.getInventory().clear();
			
			ItemStack compassMenu = new ItemStack(Material.COMPASS);
			ItemMeta compassMenuM = compassMenu.getItemMeta();
			compassMenuM.setDisplayName("Menu");
			compassMenuM.setLore(Arrays.asList("Boussole de menu", "Affiche le menu pour se give des kits", " ", "( Clique-droit )"));
			compassMenuM.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
			compassMenuM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			compassMenu.setItemMeta(compassMenuM);
			
			player.getInventory().addItem(compassMenu);
			
		}
		
		return false;
	}

}
