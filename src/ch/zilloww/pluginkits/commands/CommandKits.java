package ch.zilloww.pluginkits.commands;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CommandKits implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
		
		if(commandSender instanceof Player) {
			
			Player player = (Player) commandSender;
			
			
			player.sendMessage("�6Ouverture du menu de kits !");
			
			Inventory menuKits = Bukkit.createInventory(null, 9, "Menu Kits");
			
			// Niveau 1
			ItemStack levelOne = new ItemStack(Material.LEATHER_CHESTPLATE);
			ItemMeta levelOneM = levelOne.getItemMeta();
			levelOneM.setDisplayName("Kit niveau 1");
			levelOneM.setLore(Arrays.asList("Kit niveau 1", " ", "Comporte :", "- 1 Pomme", "- �p�e en bois", "- Armure compl�te en cuire"));
			levelOne.setItemMeta(levelOneM);
			
			menuKits.setItem(0, levelOne);
			
			
			// Niveau 2
			ItemStack levelTwo = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
			ItemMeta levelTwoM = levelTwo.getItemMeta();
			levelTwoM.setDisplayName("Kit niveau 2");
			levelTwoM.setLore(Arrays.asList("Kit niveau 2", " ", "Comporte :", "- 3 Pommes", "- �p�e en pierre", "- Armure compl�te en c�te de maille"));
			levelTwo.setItemMeta(levelTwoM);
			
			menuKits.setItem(1, levelTwo);
			
			
			// Niveau 3
			ItemStack levelThree = new ItemStack(Material.GOLD_CHESTPLATE);
			ItemMeta levelThreeM = levelThree.getItemMeta();
			levelThreeM.setDisplayName("Kit niveau 3");
			levelThreeM.setLore(Arrays.asList("Kit niveau 3", " ", "Comporte :", "- 6 Pommes", "- �p�e en or", "- Armure compl�te en or", "- 1 Pomme en or"));
			levelThree.setItemMeta(levelThreeM);
			
			menuKits.setItem(2, levelThree);
			
			
			// Niveau 4
			ItemStack levelFour = new ItemStack(Material.IRON_CHESTPLATE);
			ItemMeta levelFourM = levelFour.getItemMeta();
			levelFourM.setDisplayName("Kit niveau 4");
			levelFourM.setLore(Arrays.asList("Kit niveau 4", " ", "Comporte :", "- 10 Steaks", "- �p�e en fer", "- Armure compl�te en fer", "- 3 Pommes en or"));
			levelFour.setItemMeta(levelFourM);
			
			menuKits.setItem(3, levelFour);
			
			
			// Niveau 5
			ItemStack levelFive = new ItemStack(Material.DIAMOND_CHESTPLATE);
			ItemMeta levelFiveM = levelFive.getItemMeta();
			levelFiveM.setDisplayName("Kit niveau 5");
			levelFiveM.setLore(Arrays.asList("Kit niveau 5", " ", "Comporte :", "- 40 Steaks", "- �p�e en diamant KnockBack I", "- Armure compl�te en diamant", "- 10 Pommes en or"));
			levelFive.setItemMeta(levelFiveM);
			
			menuKits.setItem(4, levelFive);
			
			
			// Niveau OP
			ItemStack op = new ItemStack(Material.DIAMOND_SWORD);
			ItemMeta opM = op.getItemMeta();
			opM.setDisplayName("Kit OP");
			opM.setLore(Arrays.asList("Kit OP", " ", "Comporte :", "- 256 Steaks", "- �p�e en diamant Sharpness III, Unbreaking IIII, KnockBack II", "- Armure compl�te en diamant Unbreaking III, Protection IIII", "- 40 Pommes en or", "- 64 Tnt's"));
			opM.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
			opM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			op.setItemMeta(opM);
			
			menuKits.setItem(5, op);
			
			
			// Fermer le menu
			ItemStack close = new ItemStack(Material.STAINED_GLASS_PANE);
			ItemMeta closeM = close.getItemMeta();
			closeM.setDisplayName("Fermer le menu");
			closeM.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
			closeM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			close.setItemMeta(closeM);
			
			menuKits.setItem(8, close);
			
			
			// Clear
			ItemStack clear = new ItemStack(Material.ENCHANTED_BOOK);
			ItemMeta clearM = close.getItemMeta();
			clearM.setDisplayName("Clear");
			clearM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			clear.setItemMeta(clearM);
			
			menuKits.setItem(7, clear);
			
			player.openInventory(menuKits);
			
		}
		
		
		
		
		return false;
	}

}
