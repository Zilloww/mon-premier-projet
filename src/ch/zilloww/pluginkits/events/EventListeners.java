package ch.zilloww.pluginkits.events;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import ch.zilloww.pluginkits.Main;

public class EventListeners implements Listener{

	private Main main;
	
	public EventListeners(Main main) {
		
		this.main = main;
		
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		
		Player player = event.getPlayer();
		
		player.getInventory().clear();
		player.setFoodLevel(20);
		player.setHealth(20);
		player.setGameMode(GameMode.SURVIVAL);
		
		ItemStack compassMenu = new ItemStack(Material.COMPASS);
		ItemMeta compassMenuM = compassMenu.getItemMeta();
		compassMenuM.setDisplayName("Menu");
		compassMenuM.setLore(Arrays.asList("Boussole de menu", "Affiche le menu pour se give des kits", " ", "( Clique-droit )"));
		compassMenuM.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
		compassMenuM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		compassMenu.setItemMeta(compassMenuM);
		
		player.getInventory().setItem(0, compassMenu);
		
		Location spawn = new Location(player.getWorld(), 128.450, 64.1, 457.330, 179.7f, 1.1f);
		player.teleport(spawn);
		
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		
		Player player = event.getPlayer();
		
		Bukkit.broadcastMessage("Le joueur " + player.getName() + " vient de quitter la partie !");
		
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		
		Player player = event.getPlayer();
		Action action = event.getAction();
		ItemStack item = event.getItem();
		
		// Arr�ter si l'item utilis� est NULL
		if(item == null) {
			return;
		}
				
		if(item.getType() == Material.COMPASS && item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().equalsIgnoreCase("Menu") && action == Action.RIGHT_CLICK_AIR) {
			
			player.sendMessage("�6Ouverture du menu de kits !");
			
			Inventory menuKits = Bukkit.createInventory(null, 27, "Menu Kits");
			
			// Niveau 1
			ItemStack levelOne = new ItemStack(Material.LEATHER_CHESTPLATE);
			ItemMeta levelOneM = levelOne.getItemMeta();
			levelOneM.setDisplayName("Kit niveau 1");
			levelOneM.setLore(Arrays.asList("Kit niveau 1", " ", "Comporte :", "- 1 Pomme", "- �p�e en bois", "- Armure compl�te en cuire"));
			levelOne.setItemMeta(levelOneM);
			
			menuKits.setItem(0, levelOne);
			
			
			// Niveau 2
			ItemStack levelTwo = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
			ItemMeta levelTwoM = levelTwo.getItemMeta();
			levelTwoM.setDisplayName("Kit niveau 2");
			levelTwoM.setLore(Arrays.asList("Kit niveau 2", " ", "Comporte :", "- 3 Pommes", "- �p�e en pierre", "- Armure compl�te en c�te de maille"));
			levelTwo.setItemMeta(levelTwoM);
			
			menuKits.setItem(1, levelTwo);
			
			
			// Niveau 3
			ItemStack levelThree = new ItemStack(Material.GOLD_CHESTPLATE);
			ItemMeta levelThreeM = levelThree.getItemMeta();
			levelThreeM.setDisplayName("Kit niveau 3");
			levelThreeM.setLore(Arrays.asList("Kit niveau 3", " ", "Comporte :", "- 6 Pommes", "- �p�e en or", "- Armure compl�te en or", "- 1 Pomme en or"));
			levelThree.setItemMeta(levelThreeM);
			
			menuKits.setItem(2, levelThree);
			
			
			// Niveau 4
			ItemStack levelFour = new ItemStack(Material.IRON_CHESTPLATE);
			ItemMeta levelFourM = levelFour.getItemMeta();
			levelFourM.setDisplayName("Kit niveau 4");
			levelFourM.setLore(Arrays.asList("Kit niveau 4", " ", "Comporte :", "- 10 Steaks", "- �p�e en fer", "- Armure compl�te en fer", "- 3 Pommes en or"));
			levelFour.setItemMeta(levelFourM);
			
			menuKits.setItem(3, levelFour);
			
			
			// Niveau 5
			ItemStack levelFive = new ItemStack(Material.DIAMOND_CHESTPLATE);
			ItemMeta levelFiveM = levelFive.getItemMeta();
			levelFiveM.setDisplayName("Kit niveau 5");
			levelFiveM.setLore(Arrays.asList("Kit niveau 5", " ", "Comporte :", "- 40 Steaks", "- �p�e en diamant KnockBack I", "- Armure compl�te en diamant", "- 10 Pommes en or"));
			levelFive.setItemMeta(levelFiveM);
			
			menuKits.setItem(4, levelFive);
			
			
			// Niveau OP
			ItemStack op = new ItemStack(Material.DIAMOND_SWORD);
			ItemMeta opM = op.getItemMeta();
			opM.setDisplayName("Kit OP");
			opM.setLore(Arrays.asList("Kit OP", " ", "Comporte :", "- 256 Steaks", "- �p�e en diamant Sharpness III, Unbreaking IIII, KnockBack II", "- Armure compl�te en diamant Unbreaking III, Protection IIII", "- 40 Pommes en or", "- 64 Tnt's"));
			opM.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
			opM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			op.setItemMeta(opM);
			
			menuKits.setItem(5, op);
			
			
			// Fermer le menu
			ItemStack close = new ItemStack(Material.STAINED_GLASS_PANE);
			ItemMeta closeM = close.getItemMeta();
			closeM.setDisplayName("Fermer le menu");
			closeM.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
			closeM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			close.setItemMeta(closeM);
			
			menuKits.setItem(8, close);
			
			
			// Clear
			ItemStack clear = new ItemStack(Material.ENCHANTED_BOOK);
			ItemMeta clearM = close.getItemMeta();
			clearM.setDisplayName("Clear");
			clearM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			clear.setItemMeta(clearM);
			
			menuKits.setItem(7, clear);
			
			
			player.openInventory(menuKits);
				
		}
			
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		
		Player player = (Player) event.getWhoClicked();
		Inventory inventoryClicked = event.getInventory();
		ItemStack itemClicked = event.getCurrentItem();
		Inventory playerInv = player.getInventory();
		
		if(itemClicked == null) {
			return;
		}
		
		if(inventoryClicked.getName() == "Menu Kits") {
			
			event.setCancelled(true);
			player.closeInventory();
			
			switch(itemClicked.getType()) {
			
				case LEATHER_CHESTPLATE:
					playerInv.addItem(newItem(Material.APPLE, 1));
					player.getInventory().setItemInMainHand(newItem(Material.WOOD_SWORD, 1));
					player.getInventory().setHelmet(newItem(Material.LEATHER_HELMET, 1));
					player.getInventory().setChestplate(newItem(Material.LEATHER_CHESTPLATE, 1));
					player.getInventory().setLeggings(newItem(Material.LEATHER_LEGGINGS, 1));
					player.getInventory().setBoots(newItem(Material.LEATHER_BOOTS, 1));
					player.sendMessage("�2Vous venez de recevoir le kit niveau 1 !");
				break;
				case CHAINMAIL_CHESTPLATE:
					playerInv.addItem(newItem(Material.APPLE, 3));
					player.getInventory().setItemInMainHand(newItem(Material.STONE_SWORD, 1));
					player.getInventory().setHelmet(newItem(Material.CHAINMAIL_HELMET, 1));
					player.getInventory().setChestplate(newItem(Material.CHAINMAIL_CHESTPLATE, 1));
					player.getInventory().setLeggings(newItem(Material.CHAINMAIL_LEGGINGS, 1));
					player.getInventory().setBoots(newItem(Material.CHAINMAIL_BOOTS, 1));
					player.sendMessage("�2Vous venez de recevoir le kit niveau 2 !");
				break;
				case GOLD_CHESTPLATE:
					playerInv.addItem(newItem(Material.APPLE, 6));
					player.getInventory().setItemInMainHand(newItem(Material.GOLD_SWORD, 1));
					player.getInventory().setHelmet(newItem(Material.GOLD_HELMET, 1));
					player.getInventory().setChestplate(newItem(Material.GOLD_CHESTPLATE, 1));
					player.getInventory().setLeggings(newItem(Material.GOLD_LEGGINGS, 1));
					player.getInventory().setBoots(newItem(Material.GOLD_BOOTS, 1));
					playerInv.addItem(newItem(Material.GOLDEN_APPLE, 1));
					player.sendMessage("�2Vous venez de recevoir le kit niveau 3 !");
				break;
				case IRON_CHESTPLATE:
					playerInv.addItem(newItem(Material.GRILLED_PORK, 10));
					player.getInventory().setItemInMainHand(newItem(Material.IRON_SWORD, 1));
					player.getInventory().setHelmet(newItem(Material.IRON_HELMET, 1));
					player.getInventory().setChestplate(newItem(Material.IRON_CHESTPLATE, 1));
					player.getInventory().setLeggings(newItem(Material.IRON_LEGGINGS, 1));
					player.getInventory().setBoots(newItem(Material.IRON_BOOTS, 1));
					playerInv.addItem(newItem(Material.GOLDEN_APPLE, 3));
					player.sendMessage("�2Vous venez de recevoir le kit niveau 4 !");
				break;
				case DIAMOND_CHESTPLATE:
					playerInv.addItem(newItem(Material.GRILLED_PORK, 40));
					
					ItemStack diamondSword = new ItemStack(Material.DIAMOND_SWORD);
					ItemMeta diamondSwordM = diamondSword.getItemMeta();
					diamondSwordM.addEnchant(Enchantment.KNOCKBACK, 1, true);
					diamondSword.setItemMeta(diamondSwordM);
					
					player.getInventory().setItemInMainHand(diamondSword);
					player.getInventory().setHelmet(newItem(Material.DIAMOND_HELMET, 1));
					player.getInventory().setChestplate(newItem(Material.DIAMOND_CHESTPLATE, 1));
					player.getInventory().setLeggings(newItem(Material.DIAMOND_LEGGINGS, 1));
					player.getInventory().setBoots(newItem(Material.DIAMOND_BOOTS, 1));
					playerInv.addItem(newItem(Material.GOLDEN_APPLE, 10));
					player.sendMessage("�2Vous venez de recevoir le kit niveau 5 !");
				break;
				case DIAMOND_SWORD:
					playerInv.addItem(newItem(Material.GRILLED_PORK, 64));
					playerInv.addItem(newItem(Material.GRILLED_PORK, 64));
					playerInv.addItem(newItem(Material.GRILLED_PORK, 64));
					playerInv.addItem(newItem(Material.GRILLED_PORK, 64));
					
					// �p�e
					ItemStack diamondSwordOp = new ItemStack(Material.DIAMOND_SWORD);
					ItemMeta diamondSwordOpM = diamondSwordOp.getItemMeta();
					diamondSwordOpM.setLore(Arrays.asList("�p�e la plus puissante du serveur !"));
					diamondSwordOpM.addEnchant(Enchantment.KNOCKBACK, 2, true);
					diamondSwordOpM.addEnchant(Enchantment.DAMAGE_ALL, 3, true);
					diamondSwordOpM.addEnchant(Enchantment.DURABILITY, 4, true);
					diamondSwordOp.setItemMeta(diamondSwordOpM);
					player.getInventory().setItemInMainHand(diamondSwordOp);
					
					// Casque
					ItemStack helmet = new ItemStack(Material.DIAMOND_HELMET);
					ItemMeta helmetM = helmet.getItemMeta();
					helmetM.addEnchant(Enchantment.DURABILITY, 3, true);
					helmetM.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
					helmet.setItemMeta(helmetM);
					player.getInventory().setHelmet(helmet);
					
					// Plastron
					ItemStack chest = new ItemStack(Material.DIAMOND_CHESTPLATE);
					ItemMeta chestM = chest.getItemMeta();
					chestM.addEnchant(Enchantment.DURABILITY, 3, true);
					chestM.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
					chest.setItemMeta(chestM);
					player.getInventory().setChestplate(chest);

					// Jambi�res
					ItemStack legs = new ItemStack(Material.DIAMOND_LEGGINGS);
					ItemMeta legsM = legs.getItemMeta();
					legsM.addEnchant(Enchantment.DURABILITY, 3, true);
					legsM.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
					legs.setItemMeta(legsM);
					player.getInventory().setLeggings(legs);
					
					// Bottes
					ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS);
					ItemMeta bootsM = boots.getItemMeta();
					bootsM.addEnchant(Enchantment.DURABILITY, 3, true);
					bootsM.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
					boots.setItemMeta(bootsM);
					player.getInventory().setBoots(boots);

					
					playerInv.addItem(newItem(Material.GOLDEN_APPLE, 40));
					playerInv.addItem(newItem(Material.TNT, 64));
					player.sendMessage("�2Vous venez de recevoir le kit niveau �cOP !");
				break;
				case STAINED_GLASS_PANE:
					player.closeInventory();
				break;
				case ENCHANTED_BOOK:
					player.getInventory().clear();
					
					
					ItemStack compassMenu = new ItemStack(Material.COMPASS);
					ItemMeta compassMenuM = compassMenu.getItemMeta();
					compassMenuM.setDisplayName("Menu");
					compassMenuM.setLore(Arrays.asList("Boussole de menu", "Affiche le menu pour se give des kits", " ", "( Clique-droit )"));
					compassMenuM.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
					compassMenuM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
					compassMenu.setItemMeta(compassMenuM);
					
					player.getInventory().addItem(compassMenu);
					
					
					player.closeInventory();
				break;
				default:
					break;
					
			}
			
		}
		
		
	}
	
	public ItemStack newItem(Material material, int quantity) {
		
		ItemStack item = new ItemStack(material, quantity);
		return item;
		
	}
		
}
	

