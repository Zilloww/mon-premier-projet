package ch.zilloww.pluginkits;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import ch.zilloww.pluginkits.commands.CommandGiveCompass;
import ch.zilloww.pluginkits.commands.CommandKits;
import ch.zilloww.pluginkits.events.EventListeners;

public class Main extends JavaPlugin{

	@Override
	public void onEnable() {
		
		System.out.println("[ Plugin Minecraft Kits - 1.11 ] Le Plugin vient bien de démarrer !");
		
		getCommand("kits").setExecutor(new CommandKits());
		getCommand("givecompass").setExecutor(new CommandGiveCompass());
		
		// Enregistreur d'events
		PluginManager pluginManager = getServer().getPluginManager();
		pluginManager.registerEvents(new EventListeners(this), this);
		
	}
	
	@Override
	public void onDisable() {
		
		System.out.println("[ Plugin Minecraft Kits - 1.11 ] Le Plugin vient de s'arr�ter !");
		
	}
	
}